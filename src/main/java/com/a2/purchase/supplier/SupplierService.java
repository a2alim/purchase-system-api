package com.a2.purchase.supplier;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.a2.purchase.util.Response;

/**
 * @author Md. Abdul Alim
 *
 */

@Service
public class SupplierService {

	@Autowired
	private SupplierRepository supplierRepository;

	public Response gridList(HttpServletRequest request) {
		return supplierRepository.gridList(request);
	}
	
	public Response getAllList() {
		return supplierRepository.getAllList();
	}
	
	public Response save(String reqObj) {
		return supplierRepository.save(reqObj);
	}
	
	public Response update(String reqObj) {
		return supplierRepository.update(reqObj);
	}
	
	public Response findById(Long id) {
		return supplierRepository.findById(id);

	}
	
	public Response delete(Long id) {
		return supplierRepository.delete(id);
	}
	
}
