package com.a2.purchase.supplier;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.a2.purchase.util.Response;

/**
 * @author Md. Abdul Alim
 *
 */

@RestController
@RequestMapping("/api/supplier")
public class SupplierController{
	
	@Autowired
	private SupplierService supplierService;
	
	@GetMapping("/grid-list")
	public Response gridList(HttpServletRequest request) {
		return supplierService.gridList(request);
	}
	
	@GetMapping("/list")
	public Response getAllList() {
		return supplierService.getAllList();
	}
	
	@GetMapping("/find-by-id")
	public Response findById(Long id) {
		return supplierService.findById(id);
	}
	
	@PostMapping("/create")
    public Response save(@RequestBody  String reqObj) {
        return supplierService.save(reqObj);
    }
	
	@PostMapping("/update")
    public Response update(@RequestBody  String reqObj) {
        return supplierService.update(reqObj);
    }
	
	@DeleteMapping("/delete")
	public Response delete(@RequestParam("id") long reqObj) {
		return supplierService.delete(reqObj);
	}
	
}
