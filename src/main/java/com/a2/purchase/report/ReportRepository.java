package com.a2.purchase.report;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Repository;

import com.a2.purchase.base.BaseRepository;
import com.a2.purchase.purchase.PurchaseLedgerEntity;
import com.a2.purchase.purchase.PurchaseLedgerRepository;
import com.a2.purchase.util.Response;

@Repository
public class ReportRepository extends BaseRepository {

	@Autowired
	private CoreJasperService coreJasperService;

	@Autowired
	private PurchaseLedgerRepository purchaseLedgerRepository;

	public CusJasperReportDef purchaseRpt(String reqObj) throws IOException {

		JSONObject json = new JSONObject(reqObj);
		Long id = json.getLong("id");
		PurchaseLedgerEntity res = purchaseLedgerRepository.findId(id);

		PurchaseLedgerEntity rpt = new PurchaseLedgerEntity();
		List<PurchaseLedgerEntity> purchseDtlList = new ArrayList<PurchaseLedgerEntity>();
		if (res != null) {
			purchseDtlList.add(res);
		}

		Map<String, Object> parameterMap = new HashMap<String, Object>();

		CusJasperReportDef report = new CusJasperReportDef();

		report.setReportName("purchase");
		report.setOutputFilename("purchase");
		report.setReportDir(getResoucePath("/report") + "/");
		report.setReportFormat(JasperExportFormat.PDF_FORMAT);
		report.setParameters(parameterMap);
		report.setReportData(purchseDtlList);

		ByteArrayOutputStream baos = null;
		try {
			baos = coreJasperService.generateReport(report);
		} catch (Exception e) {
			e.printStackTrace();
		}

		finally {
			baos.close();
		}

		report.setContent(baos.toByteArray());
		return report;
	}

	@SuppressWarnings("unchecked")
	public CusJasperReportDef purchaseDtlRpt(String reqObj) throws IOException {

		JSONObject json = new JSONObject(reqObj);
		String fDateStr = json.getString("fromDate");
		String tDateStr = json.getString("toDate");
		String rptFormat = json.getString("rptFormat");

		PurchaseLedgerEntity rpt = new PurchaseLedgerEntity();
		List<PurchaseLedgerEntity> purchseDtlList = new ArrayList<PurchaseLedgerEntity>();

		Response res = purchaseLedgerRepository.getAllListByDate(deateParse(fDateStr, "dd/MM/yyyy"),
				deateParse(tDateStr, "dd/MM/yyyy"));

		if (res != null && res.getItems() != null && res.getItems().size() > 0) {
			purchseDtlList = res.getItems();
		}

		Map<String, Object> parameterMap = new HashMap<String, Object>();
		parameterMap.put("fromDate", deateParse(fDateStr, "dd/MM/yyyy"));
		parameterMap.put("toDate", deateParse(tDateStr, "dd/MM/yyyy"));
		
		CusJasperReportDef report = new CusJasperReportDef();

		report.setReportName("purchaseDtl");
		report.setOutputFilename("purchaseDtl");
		report.setReportDir(getResoucePath("/report") + "/");
		if(rptFormat.equalsIgnoreCase("PDF")) {
			report.setReportFormat(JasperExportFormat.PDF_FORMAT);
		}else if(rptFormat.equalsIgnoreCase("XLSX")) {
			report.setReportFormat(JasperExportFormat.XLSX_FORMAT);
		}else if(rptFormat.equalsIgnoreCase("DOCX")) {
			report.setReportFormat(JasperExportFormat.DOCX_FORMAT);
		}else if(rptFormat.equalsIgnoreCase("CSV")) {
			report.setReportFormat(JasperExportFormat.CSV_FORMAT);
		}
		report.setParameters(parameterMap);
		report.setReportData(purchseDtlList);

		ByteArrayOutputStream baos = null;
		try {
			baos = coreJasperService.generateReport(report);
		} catch (Exception e) {
			e.printStackTrace();
		}

		finally {
			baos.close();
		}

		report.setContent(baos.toByteArray());
		return report;
	}

	public static String getResoucePath(String filePath) {
		Resource resource = new ClassPathResource(filePath);
		try {
			return resource.getFile().getAbsolutePath();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
}
