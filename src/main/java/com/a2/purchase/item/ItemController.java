package com.a2.purchase.item;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.a2.purchase.util.Response;

/**
 * @author Md. Abdul Alim
 *
 */

@RestController
@RequestMapping("/api/item")
public class ItemController{
	
	@Autowired
	private ItemService purchaseLedgerService;
	
	@GetMapping("/grid-list")
	public Response gridList(HttpServletRequest request) {
		return purchaseLedgerService.gridList(request);
	}
	
	@GetMapping("/list") 
	public Response getAllList() {
		return purchaseLedgerService.getAllList();
	}
	
	@GetMapping("/find-by-id")
	public Response findById(Long id) {
		return purchaseLedgerService.findById(id);
	}
	
	@PostMapping("/create")
    public Response save(@RequestBody  String reqObj) {
        return purchaseLedgerService.save(reqObj);
    }
	
	@PostMapping("/update")
    public Response update(@RequestBody  String reqObj) {
        return purchaseLedgerService.update(reqObj);
    }
	
	@DeleteMapping("/delete")
	public Response delete(@RequestParam("id") long reqObj) {
		return purchaseLedgerService.delete(reqObj);
	}
	
}
