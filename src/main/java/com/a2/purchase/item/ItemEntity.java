package com.a2.purchase.item;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;

/**
 * @author Md. Abdul Alim
 *
 */

@Getter
@Setter
@Entity
@Table(name = "item")
public class ItemEntity implements Serializable{

	private static final long serialVersionUID = -4538971070304943261L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY) 
	@Column(name = "ITEM_NO")
	private Long itemNo;
	
	@NotNull
	@Column(name = "ITEM_ID")
	private String itemId;
	
	@Column(name = "SUPPLIER_NO")
	private Long supplierNo;
	
	@NotNull
	@Column(name = "ITEM_NAME")
	private String itemName;
	
	@Column(name = "STOCK_QTY")
	private Integer stockQty;

	@Column(name = "PURCHASE_QTY")
	private Integer purchaseQty;	

	@Column(name = "LAST_PURCHASE_QTY")
	private Integer lastPurchaseQty;
	
	@NotNull
	@Column(name = "PUR_RATE")
	private Double purRate;
	
	@Column(name = "SALSE_RATE")
	private Double salseRate;
	
	@Column(name = "ACTIVE_STATUS")
	private Integer activeStatus;
	
}
