package com.a2.purchase.item;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.a2.purchase.util.Response;

/**
 * @author Md. Abdul Alim
 *
 */

@Service
public class ItemService {

	@Autowired
	private ItemRepository purchaseLedgerRepository;

	public Response gridList(HttpServletRequest request) {
		return purchaseLedgerRepository.gridList(request);
	}
	
	public Response getAllList() {
		return purchaseLedgerRepository.getAllList();
	}
	
	public Response save(String reqObj) {
		return purchaseLedgerRepository.save(reqObj);
	}
	
	public Response update(String reqObj) {
		return purchaseLedgerRepository.update(reqObj);
	}
	
	public Response findById(Long id) {
		return purchaseLedgerRepository.findById(id);

	}
	
	public Response delete(Long id) {
		return purchaseLedgerRepository.delete(id);
	}
	
}
