package com.a2.purchase;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;

/**
 * @author Md. Abdul Alim
 *
 */

@SpringBootApplication
@ComponentScan({"com.a2.purchase"})
public class PurchaseSystemApp extends SpringBootServletInitializer {

	public static void main(String[] args) {
		System.out.println( "Purchase System Service App Start Running...!" );
		SpringApplication.run(PurchaseSystemApp.class, args);
		System.out.println( "Purchase System Service App Running Successfully...!" );
	}

}
