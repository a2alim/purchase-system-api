package com.a2.purchase.purchase;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Random;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.castor.core.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import com.a2.purchase.base.BaseRepository;
import com.a2.purchase.item.ItemRepository;
import com.a2.purchase.pagination.DataTableRequest;
import com.a2.purchase.pagination.DataTableResults;
import com.a2.purchase.pagination.PaginationCriteria;
import com.a2.purchase.purchase.dtl.PurchadeLedgerDtlEntity;
import com.a2.purchase.util.CommonUtils;
import com.a2.purchase.util.Response;

/**
 * @author Md. Abdul Alim
 *
 */

@Repository
@Transactional
public class PurchaseLedgerRepository extends BaseRepository {

	@Autowired
	private ItemRepository itemRepository;

	// Grid List
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Response gridList(HttpServletRequest request) {

		Response response = new Response();
		DataTableResults<PurchaseLedgerEntity> dataTableResults = null;

		PurchaseLedgerEntity purchaseLedgerObj = new PurchaseLedgerEntity();
		DataTableRequest dataTableInRQ = new DataTableRequest(request);
		List gridList = new ArrayList<>();

		String fDateStr = request.getParameter("fromDate");
		String tDateStr = request.getParameter("toDate");

		if (!StringUtils.isEmpty(fDateStr)) {
			purchaseLedgerObj.setFromDate(deateParse(fDateStr, "dd/MM/yyyy"));
		}

		if (!StringUtils.isEmpty(tDateStr)) {
			purchaseLedgerObj.setToDate(deateParse(tDateStr, "dd/MM/yyyy"));
		}

		Long totalRowCount = totalCount(purchaseLedgerObj);
		response = baseList(typedQuery(purchaseLedgerObj, dataTableInRQ));
		if (response.isSuccess()) {
			if (response.getItems() != null) {
				gridList = response.getItems();
			}
			dataTableResults = dataTableResults(dataTableInRQ, countTypedQuery(purchaseLedgerObj, dataTableInRQ),
					gridList, totalRowCount);
		}
		response.setItems(null);
		response.setObj(dataTableResults);
		return response;
	}

	// Get All List
	public Response getAllList() {
		PurchaseLedgerEntity purchaseLedgerObj = new PurchaseLedgerEntity();
		return baseList(criteriaQuery(purchaseLedgerObj));
	}

	// Get All List
	public Response getAllListByDate(Date fromDate,Date toDate) {
		PurchaseLedgerEntity purchaseLedgerObj = new PurchaseLedgerEntity();
		purchaseLedgerObj.setFromDate(fromDate);
		purchaseLedgerObj.setToDate(toDate);
		return baseList(criteriaQuery(purchaseLedgerObj));
	}

	// findById or any other response type
	public Response findById(Long id) {
		Response response = new Response();
		PurchaseLedgerEntity purchaseLedgerObj = new PurchaseLedgerEntity();

		purchaseLedgerObj.setLedgNo(id);
		response = baseList(criteriaQuery(purchaseLedgerObj));
		if (response.isSuccess()) {
			return response;
		}
		return getErrorResponse("Record not Found !!");
	}

	// FindById Entity Type
	public PurchaseLedgerEntity findId(Long id) {
		PurchaseLedgerEntity purchaseLedgerObj = new PurchaseLedgerEntity();

		purchaseLedgerObj.setLedgNo(id);
		Response response = baseFindById(criteriaQuery(purchaseLedgerObj));
		if (response.isSuccess()) {
			return getValueFromObject(response.getObj(), PurchaseLedgerEntity.class);
		}
		return null;
	}

	// Save
	public Response save(String reqObj) {
		Response response = new Response();
		PurchaseLedgerEntity purchaseLedgerObj = objectMapperReadValue(reqObj, PurchaseLedgerEntity.class);

		if (purchaseLedgerObj == null) {
			return getErrorResponse("Data not found for Save!");
		}
		Random r = new Random();
		String purchaseId = "PUR100" + r.nextInt(99999 - 10000);
		purchaseLedgerObj.setPurchaseId(purchaseId);

		response = baseOnlySave(purchaseLedgerObj);

		if (response.isSuccess()) {

			for (PurchadeLedgerDtlEntity dtlObj : purchaseLedgerObj.getItemDtlList()) {
				itemRepository.updateItem(dtlObj);
			}

			response.setMessage("Successsfully Data Saved!");
			return response;
		}
		return getErrorResponse("Data Not Saved!");
	}

	// Update
	public Response update(String reqObj) {
		Response response = new Response();
		PurchaseLedgerEntity purchaseLedgerObj = objectMapperReadValue(reqObj, PurchaseLedgerEntity.class);

		PurchaseLedgerEntity obj = findId(purchaseLedgerObj.getLedgNo());
		if (obj != null) {
			response = baseUpdate(purchaseLedgerObj);
			if (response.isSuccess()) {

				for (PurchadeLedgerDtlEntity dtlObj : purchaseLedgerObj.getItemDtlList()) {
					int lastPurQty = dtlObj.getLastPurchaseQty();
					int purQty = dtlObj.getPurchaseQty();
					dtlObj.setAddStockQty(purQty-lastPurQty);
					dtlObj.setLastPurchaseQty(dtlObj.getPurchaseQty());
					itemRepository.updateItem(dtlObj);
				}

				response.setMessage("Successsfully Data Updated!");
				return response;
			}
		}
		return getErrorResponse("Data not Found for Update!");
	}

	// Delete
	public Response delete(Long id) {
		PurchaseLedgerEntity purchaseLedgerObj = findId(id);
		if (purchaseLedgerObj == null) {
			return getErrorResponse("Record not found!");
		}
		return baseDelete(purchaseLedgerObj);
	}

	private Long totalCount(PurchaseLedgerEntity filter) {
		CriteriaBuilder builder = criteriaBuilder();
		CriteriaQuery<Long> criteriaQuery = longCriteriaQuery(builder);
		Root<PurchaseLedgerEntity> root = from(PurchaseLedgerEntity.class, criteriaQuery);
		return totalCount(builder, criteriaQuery, root, criteriaCondition(filter, builder, root));
	}

	@SuppressWarnings({ "unchecked" })
	private List<Predicate> criteriaCondition(PurchaseLedgerEntity filter, CriteriaBuilder builder,
			Root<PurchaseLedgerEntity> root) {
		if (builder == null) {
			builder = super.builder;
		}
		if (root == null) {
			root = super.root;
		}
		List<Predicate> p = new ArrayList<Predicate>();
		if (filter != null) {
			if (filter.getLedgNo() != null && filter.getLedgNo() > 0) {
				p.add(builder.equal(root.get("ledgNo"), filter.getLedgNo()));
			}
		}
		if (filter.getFromDate() != null && filter.getToDate() != null) {
			Date fromDate = addHourMinutesSeconds(00, 00, 00, filter.getFromDate());
			Date toDate = addHourMinutesSeconds(23, 59, 59, filter.getToDate());
			p.add(builder.between(root.get("purchaseDate"), fromDate, toDate));
		}
		return p;
	}

	@SuppressWarnings({ "unused", "rawtypes" })
	private void init() {
		initEntityManagerBuilderCriteriaQueryRoot(PurchaseLedgerEntity.class);
		CriteriaBuilder builder = super.builder;
		CriteriaQuery criteria = super.criteria;
		Root root = super.root;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private CriteriaQuery criteriaQuery(PurchaseLedgerEntity filter) {
		init();
		List<Predicate> p = new ArrayList<Predicate>();
		p = criteriaCondition(filter, null, null);

		if (!CollectionUtils.isEmpty(p)) {
			Predicate[] pArray = p.toArray(new Predicate[] {});
			Predicate predicate = builder.and(pArray);
			criteria.where(predicate);
		}
		return criteria;
	}

	@SuppressWarnings({ "rawtypes" })
	private <T> TypedQuery typedQuery(PurchaseLedgerEntity filter, DataTableRequest<T> dataTableInRQ) {
		init();
		List<Predicate> pArrayJoin = new ArrayList<Predicate>();
		List<Predicate> pConjunction = criteriaCondition(filter, null, null);
		List<Predicate> pDisJunction = dataTablefilter(dataTableInRQ);
		Predicate predicateAND = null;
		Predicate predicateOR = null;

		if (!CollectionUtils.isEmpty(pConjunction)) {
			Predicate[] pArray = pConjunction.toArray(new Predicate[] {});
			predicateAND = builder.and(pArray);
		}
		if (!CollectionUtils.isEmpty(pDisJunction)) {
			Predicate[] pArray = pDisJunction.toArray(new Predicate[] {});
			predicateOR = builder.or(pArray);
		}
		if (predicateAND != null) {
			pArrayJoin.add(predicateAND);
		}
		if (predicateOR != null) {
			pArrayJoin.add(predicateOR);
		}
		if (dataTableInRQ.getOrder().getName() != null && !dataTableInRQ.getOrder().getName().isEmpty()) {
			if (dataTableInRQ.getOrder().getSortDir().equals("ASC")) {
				criteria.orderBy(builder.asc(root.get(dataTableInRQ.getOrder().getName())));
			} else {
				criteria.orderBy(builder.desc(root.get(dataTableInRQ.getOrder().getName())));
			}

		}
		criteria.where(pArrayJoin.toArray(new Predicate[0]));
		return baseTypedQuery(criteria, dataTableInRQ);
	}

	private <T> Long countTypedQuery(PurchaseLedgerEntity filter, DataTableRequest<T> dataTableInRQ) {

		if (dataTableInRQ.getPaginationRequest().isFilterByEmpty()) {
			return 0l;
		}

		CriteriaBuilder builder = criteriaBuilder();
		CriteriaQuery<Long> criteriaQuery = longCriteriaQuery(builder);
		Root<PurchaseLedgerEntity> root = from(PurchaseLedgerEntity.class, criteriaQuery);
		return totalCount(builder, criteriaQuery, root, criteriaCondition(filter, builder, root),
				dataTablefilter(dataTableInRQ, builder, root));
	}

	@Override
	public <T> List<Predicate> dataTablefilter(DataTableRequest<T> dataTableInRQ, CriteriaBuilder builder, Root root) {
		PaginationCriteria paginationCriteria = dataTableInRQ.getPaginationRequest();
		paginationCriteria.getFilterBy().getMapOfFilters();
		List<Predicate> p = new ArrayList<Predicate>();

		if (!paginationCriteria.isFilterByEmpty()) {
			Iterator<Entry<String, String>> fbit = paginationCriteria.getFilterBy().getMapOfFilters().entrySet()
					.iterator();
			while (fbit.hasNext()) {
				Map.Entry<String, String> pair = fbit.next();
				if (!pair.getKey().equals("ssModifiedOn")) {

					// System.out.println("pair.getKey() " + pair.getKey());

					p.add(builder.like(builder.lower(root.get(pair.getKey())),
							CommonUtils.PERCENTAGE_SIGN + pair.getValue().toLowerCase() + CommonUtils.PERCENTAGE_SIGN));
				}
			}

		}
		return p;
	}

}
