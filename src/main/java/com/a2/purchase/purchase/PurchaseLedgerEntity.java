package com.a2.purchase.purchase;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import com.a2.purchase.purchase.dtl.PurchadeLedgerDtlEntity;

import lombok.Getter;
import lombok.Setter;

/**
 * @author Md. Abdul Alim
 *
 */

@Getter
@Setter
@Entity
@Table(name = "purchase_ledger")
public class PurchaseLedgerEntity implements Serializable{

	private static final long serialVersionUID = -4538971070304943261L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY) 
	@Column(name = "LEDG_NO")
	private Long ledgNo;
	
	@Column(name = "SUPPLIER_NO")
	private String supplierNo;
	
	
	@Column(name = "SUPPLIER_NAME")
	private String supplierName;
	
	@NotNull
	@Column(name = "INVOICE_NO")
	private String invoiceNo;
	
	@NotNull
	@Column(name = "PURCHASE_ID")
	private String purchaseId;
	
	@Column(name = "PAY_TYPE_NO")
	private Integer payTypeNo;
	
	@Column(name = "PURCHASE_DATE")
	private Date purchaseDate;

	@Column(name = "DETAILS")
	private String details;

	@Column(name = "TOTAL_AMT")
	private Double totalAmt;
	
	@Column(name = "DISCOUNT_AMT")
	private Double discountAmt;
	
	@Column(name = "GRAND_TOTAL")
	private Double grandTotal;
	
	@Column(name = "PAID_AMT")
	private Double paidAmt;
	
	@Column(name = "DUE_AMT")
	private Double dueAmt;
	
	@OneToMany(cascade = CascadeType.ALL)  
	@JoinColumn(name="LEDG_NO")  
//	@OneToMany(mappedBy = "LEDG_NO")
	private List<PurchadeLedgerDtlEntity> itemDtlList;
	
	
	@Transient
	private Date fromDate;

	@Transient
	private Date toDate;
	
	  
}
