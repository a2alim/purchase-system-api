package com.a2.purchase.purchase.dtl;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Entity
@Table(name = "purchase_ledger_dtl")
public class PurchadeLedgerDtlEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "LEDG_DTL_NO")
	private Long ledgDtlNo;

//	@ManyToOne(fetch = FetchType.LAZY)
	@Column(name = "LEDG_NO")
	private Long ledgNo;

	@Column(name = "ITEM_NO")
	private Long itemNo;

	@Column(name = "ITEM_NAME")
	private String itemName;

	@Column(name = "PURCHASE_QTY")
	private Integer purchaseQty;
	
	@Column(name = "LAST_PURCHASE_QTY")
	private Integer lastPurchaseQty;

	@Column(name = "PUR_RATE")
	private Double purRate;

	@Column(name = "TOTAL_AMT")
	private Double totalAmt;
	
	@Transient
	private Integer addStockQty=0;
	

}
